require 'open-uri'
require 'csv'
require 'net/ftp'

module Crawler
  class BaseCrawler

    # handy to peek into what the browser is doing right now
    def screenshot(name = "screenshot")
      page.driver.render("public/#{name}.jpg",full: true)
    end

    def visit_url(url)
      begin
        response = Nokogiri.parse(open(url))
        write_log "visited #{url}"
        sleep(1)
        @doc = response
      rescue StandardError => e
        write_log("#{e.message} #{url}")
        nil
      end
    end

    def export_csv(data, file_name)
      write_log "Begin export csv ...."
      Dir.mkdir(self.dirpath) unless File.exist?(dirpath)

      csv_file = File.join(self.dirpath, file_name)
      CSV.open(csv_file, "a+", {col_sep: "\t"}) do |csv|
        csv << data.first.keys if csv.count == 0
        data.each do |hash|
          csv << hash.values
        end
      end
      write_log "Export file '#{file_name}' complete total #{data.count} row"
    end

    def dirpath
      File.expand_path(File.join(Rails.root, 'tmp', 'export'))
    end

    def write_log(message)
      puts message
      @logger ||= Logger.new('log/crawler.log')
      @logger.info message
    end

    def ftp_config
      fail '@overwrite'
    end

    def upload_csv_to_ftp(file_path)
      return unless ftp_config['enable']
      ftp = Net::FTP.new(
        ftp_config['host'],
        ftp_config['username'],
        ftp_config['password']
      )

      ftp.binary  = true
      ftp.passive = true
      
      if ftp_config['dir'].present?
        remote_dir = "#{ftp_config['dir']}/#{site_name}"
      else
        remote_dir = site_name
      end
      begin
        ftp.chdir remote_dir
      rescue
        # if the remote dir doesn't exist, we create it
        ftp.mkdir remote_dir
        ftp.chdir remote_dir
      end
    
      ftp.put(file_path)
      ftp.close
      write_log "uploaded data to ftp :#{Time.zone.now}"
      write_log file_path
    rescue => e
      write_log(e.message)
    end
  end
end
