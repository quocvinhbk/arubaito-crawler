module Crawler
  class SiteCrawler < BaseCrawler
    def initialize(params)
      super()
      @site_name = params[:site_name]
      @site_config = params[:site_config]
      @ftp_config = params[:ftp]
    end

    def excute
      FileUtils.rm_rf Dir.glob(self.dirpath)
    end

    def ftp_config
      @ftp_config
    end

    def site_name
      @site_name
    end

    def upload_ftp(file_name)
      file_path = "#{dirpath}/#{file_name}.csv"
      upload_csv_to_ftp(file_path)
    end

    def export data, file_name
      export_csv(data, "#{file_name}.csv")
      upload_ftp(file_name)
    end

    def dirpath
      File.expand_path(File.join(Rails.root, 'tmp', 'export', @site_name))
    end
  end
end
