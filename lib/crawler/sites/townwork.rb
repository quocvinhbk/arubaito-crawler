module Crawler
  class Sites::Townwork < SiteCrawler
  	
    def excute
      super
      define_instance_methods
    end
    def crawl_categories
      visit_url categories_config('url')
      @doc.xpath(categories_config('group')).map do |cate|
        crawl_category(cate)
      end
    end

    def crawl_category(cate)
      name = cate.search(categories_config('name')).text
      translate_name = categories_config('map')[name] || "other"
      data = []
      cate.search(categories_config('links')).each do |a| 
        row = {}
        row[:name] = a.text
        data << row
      end
      file_name = "occupation_#{translate_name}"
      export data, file_name
    end

    def crawl_merits
      visit_url merits_config('url')
    	data = []
      @doc.xpath(merits_config('links')).each do |a| 
        row = {}
        row[:name] = a.text
        data << row
      end
      file_name = merits_config('file_name')
      export data, file_name
    end

    def crawl_themes
    	visit_url themes_config('url')
      data = []
      @doc.xpath(themes_config('links')).each do |a| 
        row = {}
        row[:name] = a.text
        data << row
      end
      file_name = themes_config('file_name')
      export data, file_name
    end

    def crawl_brands
    	visit_url brands_config('url')
      data = []
      @doc.xpath(brands_config('list')).each do |brand_url|
        visit_url @site_config['base_url'] + brand_url[:href]
        @doc.xpath(brands_config('links')).each do |a| 
          
          row = {}
          row[:name] = a.text
          data << row
        end
      end
      file_name = brands_config('file_name')
      export data, file_name
    end

    def define_instance_methods
      [:categories, :merits, :themes, :brands].each do |method_name|
        self.class.send :define_method, "#{method_name}_config" do |arg|
          @site_config[method_name.to_s][arg]
        end
        send("crawl_#{method_name.to_s}")
      end
    end
  end
end