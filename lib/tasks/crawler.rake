namespace :crawler do
  task site: :environment do
    if ENV['site'].present?
      start_crawler_site(ENV['site'])
    else
      config['sites'].keys.each do |site_name|
        start_crawler_site(site_name)
      end
    end
  end

  def start_crawler_site(site_name)
    params = {}
    params[:site_name] = site_name
    params[:site_config] = config['sites'][site_name]
    params[:ftp] = config['ftp']
    site = if File.exist?("#{Rails.root}/lib/crawler/sites/#{site_name}.rb")
                "Crawler::Sites::#{site_name.camelize}".constantize
              else
                Crawler::SiteCrawler
              end
    site.new(params).excute
  end

  def config
    @config ||= YAML.load_file("#{Rails.root}/lib/crawler/sites.yml")
  end
end
